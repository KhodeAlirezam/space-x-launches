import "./LaunchListPage.css";

import { API_ROUTES, ROUTES } from "../consts/Routes";
import useLaunches from "../hooks/useLaunches";
import LaunchCard from "../components/LaunchCard/LaunchCard";

import { useStateValue } from "../SearchStateContext";

const LaunchListPage = (props) => {
  const path = props.location.pathname;
  const [{ searchKey }] = useStateValue();
  const launches = useLaunches(
    searchKey ? API_ROUTES[ROUTES.SEARCH](searchKey) : API_ROUTES[path]
  );

  return (
    <div className="launch-list">
      {launches.isError && (
        <div className="failed">Failed to fetch launches</div>
      )}

      {launches.isLoading && <div className="loading">Loading...</div>}

      {!launches.isLoading &&
        launches.launchList.length === 0 &&
        !launches.isError && <div className="not-found">No launches found</div>}

      {launches.launchList.map((launch) => (
        <LaunchCard
          key={launch.flight_number + launch.mission_name}
          data={launch}
        />
      ))}
    </div>
  );
};

export default LaunchListPage;
