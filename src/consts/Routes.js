export const ROUTES = {
  PAST: "/past",
  UPCOMING: "/upcoming",
  HOME: "/",
};

export const API_ROUTES = {
  [ROUTES.PAST]: "https://api.spacexdata.com/v3/launches/past",
  [ROUTES.UPCOMING]: "https://api.spacexdata.com/v3/launches/upcoming",
  [ROUTES.HOME]: "https://api.spacexdata.com/v3/launches/past",
  [ROUTES.SEARCH]: (search) => `https://api.spacexdata.com/v3/launches/?site_name=${search}`,
};
