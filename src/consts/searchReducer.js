import { ACTIONS } from "./actions";

export const searchReducer = (state, action) => {
  if (
    action.value !== state.searchKey &&
    action.type === ACTIONS.CHANGE_SEARCH_KEY
  )
    return { searchKey: action.value };
  if (state.value !== "" && action.type === ACTIONS.CLEAR_SEARCH_KEY)
    return { searchKey: "" };
};
