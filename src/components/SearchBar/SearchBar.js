import "./SearchBar.css";

import { useStateValue } from "../../SearchStateContext";
import { ACTIONS } from "../../consts/actions";

const SearchBar = (props) => {
  const [{ searchKey }, dispatch] = useStateValue();

  const onSearch = (e) => {
    dispatch({
      type: ACTIONS.CHANGE_SEARCH_KEY,
      value: e.target.value,
    });
  };

  return (
    <input
      placeholder="Search By Site Name"
      className="search-bar"
      value={searchKey}
      onChange={onSearch}
    />
  );
};

export default SearchBar;
