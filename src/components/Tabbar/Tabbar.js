import "./Tabbar.css";

import { NavLink } from "react-router-dom";
import { ROUTES } from "../../consts/Routes";

import { useStateValue } from "../../SearchStateContext";
import { ACTIONS } from "../../consts/actions";

const Tabbar = (props) => {
  const isPastLinkActive = (match, location) => {
    if (
      location.pathname === ROUTES.HOME ||
      location.pathname === ROUTES.PAST
    ) {
      return true;
    }
    return false;
  };

  const [, dispatch] = useStateValue();

  const handleClick = () => {
    dispatch({
      type: ACTIONS.CLEAR_SEARCH_KEY,
    });
  };

  return (
    <nav className="nav">
      <ul>
        <li onClick={handleClick}>
          <NavLink
            className="Nav-link"
            to={ROUTES.PAST}
            isActive={isPastLinkActive}
          >
            PAST
          </NavLink>
        </li>
        <li onClick={handleClick}>
          <NavLink className="Nav-link" exact to={ROUTES.UPCOMING}>
            UPCOMING
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Tabbar;
