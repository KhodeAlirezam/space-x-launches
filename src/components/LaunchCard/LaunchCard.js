import "./LaunchCard.css";

const LaunchCard = (props) => {
  const launchData = props.data;
  return (
    <div className="card">
      {launchData.links.flickr_images[0] && (
        <img
          src={launchData.links.flickr_images[0]}
          alt={launchData.mission_name}
        />
      )}
      <ul className="card-cap">
        <li>
          <p>Mission Name: </p>
          <p className="launch-span">{launchData.mission_name}</p>
        </li>

        <li>
          <p>Flight Number:</p>
          <p className="launch-span">{launchData.flight_number}</p>
        </li>
        <li>
          <p>Launch Year:</p>
          <p className="launch-span">{launchData.launch_year}</p>
        </li>
        <li>
          <p>Launch Date:</p>
          <p className="launch-span">
            {`${new Date(launchData.launch_date_unix)}`}
          </p>
        </li>
        <li>
          <p>Rocket Name:</p>
          <p className="launch-span">{launchData.rocket.rocket_name}</p>
        </li>
        <li>
          <p>Launch Site Name:</p>
          <p className="launch-span">{launchData.launch_site.site_name}</p>
        </li>
      </ul>
    </div>
  );
};

export default LaunchCard;
