import "./Header.css";
import logo from "../../assets/logo.png";

import Tabbar from "../Tabbar/Tabbar";
import SearchBar from "../SearchBar/SearchBar";

const Header = (props) => {
  return (
    <header className="Header">
      <img className="logo" src={logo} alt="space x logo" />
      <Tabbar />
      <SearchBar />
    </header>
  );
};

export default Header;
