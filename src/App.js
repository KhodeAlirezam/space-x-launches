import "./App.css";

import { BrowserRouter as Router, Route } from "react-router-dom";
import { ROUTES } from "./consts/Routes";

import Header from "./components/Header/Header";
import LaunchListPage from "./pages/LaunchListPage";

import { StateProvider } from "./SearchStateContext";
import { searchReducer } from "./consts/searchReducer";

const App = () => {
  const initialSearchState = {
    searchKey: "",
  };

  return (
    <StateProvider initialState={initialSearchState} reducer={searchReducer}>
      <div className="Container">
        <Router>
          <Header />
          <Route exact path={ROUTES.PAST} component={LaunchListPage} />
          <Route exact path={ROUTES.UPCOMING} component={LaunchListPage} />
          <Route exact path={ROUTES.HOME} component={LaunchListPage} />
        </Router>
      </div>
    </StateProvider>
  );
};

export default App;
