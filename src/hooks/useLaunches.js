import { useEffect, useState } from "react";

const useLaunches = (url) => {
  const [launches, setLaunches] = useState({
    isLoading: true,
    isError: false,
    launchList: [],
  });

  useEffect(() => {
    const fetchLaunches = async () => {
      setLaunches({
        launchList: [],
        isError: false,
        isLoading: true,
      });

      try {
        const launchesRaw = await fetch(url);
        const launchList = await launchesRaw.json();
        setLaunches({
          launchList,
          isLoading: false,
          isError: false,
        });
      } catch (error) {
        setLaunches({
          launchList: [],
          isLoading: false,
          isError: true,
        });
        console.log("Failed to fetch launches", error);
      }
    };
    fetchLaunches();
  }, [url]);

  return launches;
};

export default useLaunches;
